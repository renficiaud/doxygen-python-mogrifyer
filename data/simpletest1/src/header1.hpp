#ifndef HEADER1_HPP__
#define HEADER1_HPP__

#include <vector>

//! Global typedef
typedef float general_t;

namespace funcs
{
  //!@brief Returns the value of func 1
  //!
  //! This is the value of func1
  //! @param[in] value this is the value
  void func1(general_t value);

  //!@brief Interesting s1
  struct s1
  {
    //! local typedef
    typedef general_t value_type;

    //! operator functor
    bool operator()(value_type b) { return b; }
  };
}

#endif

