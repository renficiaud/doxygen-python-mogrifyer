#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
from xml.dom.minidom import parse

from documentation_node import DocumentationNode


def parse_options():
    from optparse import OptionParser

    parser = OptionParser()
    parser.add_option("-d", "--docs", dest="docs", default="docs/xml/",
                      help="Get documentation from this directory. (Default: docs/xml/)")
    parser.add_option("-o", "--output", dest="output", default="wiki/",
                      help="Place the generated wiki pages in this directory. (Default: wiki/)")
    parser.add_option("-p", "--prefix", dest="prefix", default="Doxygen",
                      help="Use this prefix when generating wiki pages. (Default: Doxygen)")
    parser.add_option("-l", "--label", dest="labels", default=[], action="append",
                      help="Apply this label to all pages.")
    parser.add_option("-n", "--no-labels", dest="no_labels", default=False, action="store_true",
                      help="Don't apply any labels to the pages. (Overrides any --label options)")
    parser.add_option("-t", "--html", dest="html", default=None, action="store",
                      help="The location of the Doxygen generated HTML files.")
    parser.add_option("-i", "--image", dest="images", default=[], action="append",
                      help="The directory containing images used in the documentation.")
    parser.add_option("-v", "--verbose", dest="verbose", default=False, action="store_true",
                      help="Make lots of noise.")

    (options, args) = parser.parse_args()

    if not options.docs.endswith("/"):
        options.docs = options.docs + "/"
    if not options.output.endswith("/"):
        options.output = options.output + "/"
    if options.html is None:
        options.html = options.docs + "../html/"
    if options.html and not options.html.endswith("/"):
        options.html = options.html + "/"
    for i in range(len(options.images)):
        if not options.images[i].endswith("/"):
            options.images[i] = options.images[i] + "/"

    return options


from doxygen import Doxygen


def getPreviousFiles():
    try:
        return [x.strip() for x in open("%s%s.status" % (options.output, options.prefix), "r").readlines()]
    except IOError:
        return None


def main():
    options = parse_options()
    doxygen = Doxygen(options)

    root_documentation = DocumentationNode()

    doxygen.processFile(parse(os.path.join(options.docs, "index.xml")), root_documentation)

    final_doc = root_documentation.createDocumentation(0)

    print final_doc
    return

    files = []

    for type_, file_, code in doxygen.createfiles():
        if type == "wiki":
            file_ = file_ + ".wiki"
            files.append(file_)

            if options.verbose:
                print "Generating page for " + file_
            text = unicode(code)
            text += doxygen.getFooter()

            of = os.path.join(options.output, file_)
            if(not os.path.exists(os.path.dirname(of))):
                os.makedirs(os.path.dirname(of))

            open(of, "w").write(text.encode("utf-8"))
        else:
            files.append(file_)

    # Create our own default index page
    if options.prefix + ".wiki" not in files:
        file_ = options.prefix + ".wiki"
        files.append(file_)

        text = doxygen.getFooter()

        open(options.output + file_, "w").write(text.encode("utf-8"))

main()
