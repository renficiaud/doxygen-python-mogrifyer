# -*- coding: UTF-8 -*-


from .utils import getText, make_rest_title
from .text_elements import convertLine
from .member_function import DoxygenMemberFunction, DoxygenMember
from xml.dom.minidom import parseString

from documentation_node import DocumentationElement, DocumentationNode


class DoxygenClass(DocumentationElement):
    """A compound documentation element representing a class"""

    def __init__(self, xml, options, this_node):

        # get the id of the current class
        try:
            self.refid = xml.attributes["refid"].value
        except KeyError:
            self.refid = xml.attributes["id"].value

        this_node.set_id(self.refid)
        self.name = getText(xml.getElementsByTagName("compoundname")[0].childNodes)

        if options.verbose:
            print "Generating documentation for class", self.name

        raw_input('class %s' % self.name)

        l = [""]
        [x.getLines(l) for x in convertLine(xml.getElementsByTagName("briefdescription")[0], self, options, this_node)]
        self.brief = "".join(l).strip()

        l = [""]
        [x.getLines(l) for x in convertLine(xml.getElementsByTagName("detaileddescription")[0], self, options, this_node)]
        self.detailed = "".join(l).strip()

        self.members = []
        for node in xml.getElementsByTagName("memberdef"):  # if node.attributes["kind"].value == "function"]
            current_node = DocumentationNode()
            current_doc_element = DoxygenMember(node, options, current_node)
            current_node.node = current_doc_element
            this_node.children.append(current_node)
            self.members.append(current_doc_element)

    def getFileName(self):
        """Returns the filename of the current class"""
        return self.refid.replace('/', '_') + '.txt'

    def createCode(self, level=0):
        """"""

        typedefs = [i for i in self.members if i.type == 'typedef']
        public_members = [i for i in self.members if i.type == 'function' and i.protection == 'public']
        protected_members = [i for i in self.members if i.type == 'function' and i.protection == 'protected']

        raw_input(typedefs)
        out = make_rest_title(self.name, level) + '\n'

        out += self.brief + '\n' * 2
        out += self.detailed + '\n' * 2

        # typedefs
        if(len(typedefs) > 0):
            out += make_rest_title("Typedefs", level + 1) + '\n'

            for i in typedefs:
                out += i.doc + '\n'  #createCode(level+2)

        # public
        if(len(public_members) > 0):
            out += make_rest_title("Public member methods", level + 1) + '\n'

            for i in public_members:
                out += i.doc + '\n'  #createCode(level+2)

        print out
        return out

    def createFiles(self,):
        return (self.getFileName(), self.createCode(0))
