# -*- coding: UTF-8 -*-


class DocumentationElement(object):
    """This class describes the concept behind each documentation element.
    Every documentation element is part of the documentation tree. It should implement the single
    two member functions:
    - createDocumentationBegin and
    - createDocumentationEnd,
    that will be eventually called by the generating part of the documentation

    Not storing the xml in the instance to avoid having everything in memory? Is that a good idea?
    """

    def __init__(self, node, xmlroot=None, options=None):
        self.child_nodes = None
        self.brief = u''
        self.detailed = u''
        self.xmlroot = xmlroot
        self.options = options
        self.node = node

        if not node is None:
            node.documentation = self

        pass

    def get_direct_children_and_tags(self):
        self.child_nodes = [(i, i.tagName)  for i in self.xmlroot.childNodes if i.nodeType == i.ELEMENT_NODE]
        return self.child_nodes

    def get_brief(self):
        from .text_elements import get_text_content
        self.brief = u''
        for i in (j for j in self.child_nodes if j[1] == "briefdescription"):
            self.brief += get_text_content(i[0], None, self.options, self.node)

    def get_detailed(self):
        from .text_elements import get_text_content
        self.detailed = DocumentationNode()
        self.node.add_child_node(self.detailed)
        for i in (j for j in self.child_nodes if j[1] == "detaileddescription"):
            #self.detailed += get_text_content(i[0], self.detailed, self.options, self.node)
            get_text_content(i[0], self.options, self.detailed)

    def createDocumentationBegin(self, current_depth):
        return u""

    def createDocumentation(self, current_depth):
        return u""

    def createDocumentationEnd(self, current_depth):
        return u""


class DocumentationNode(object):
    """Describes an element of the documentation tree. This does not include the cross-references"""

    known_ids = {}
    parent_to_child_relationship = {}
    node_types = {}

    node_type_group = 1
    node_type_class = 2
    node_type_func = 3

    _supported_types = (node_type_group, node_type_class, node_type_func)

    def __init__(self):
        self._id = None
        self.documentation_element = None
        #self.children = []
        #self.parents = []

    @staticmethod
    def clear_state():
        DocumentationNode.parent_to_child_relationship = {}
        DocumentationNode.known_ids = {}
        DocumentationNode.node_types = {}

    @staticmethod
    def get_node_with_id(id):
        return DocumentationNode.known_ids.get(id)

    @staticmethod
    def register_parent_to_child_relationship(parent_id, child_id):
        """Registers two nodes as being parent-child"""
        if parent_id is None:
            return
        if child_id is None:
            return

        current = DocumentationNode.parent_to_child_relationship.get(parent_id)
        if current is None:
            current = set()
            DocumentationNode.parent_to_child_relationship[parent_id] = current

        current.add(child_id)
        print 'parent<->child %s <-> %s' % (parent_id, child_id)

    @staticmethod
    def register_id_type(id, type_):
        assert(type_ in DocumentationNode._supported_types)
        assert(not DocumentationNode.node_types.has_key(id))
        DocumentationNode.node_types[id] = type_

    def add_child_node(self, child):
        #self.children.append(child)
        #child.parents.append(self)
        self.register_parent_to_child_relationship(self._id, child._id)

    @property
    def documentation(self):
        return self.documentation_element

    @documentation.setter
    def documentation(self, doc_element):
        self.documentation_element = doc_element

    @property
    def children_ids(self):
        return self.parent_to_child_relationship.get(self._id, set())

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of the current documentation_element and registers it"""
        self._id = id
        assert(not DocumentationNode.known_ids.has_key(id))
        DocumentationNode.known_ids[id] = self

    def createDocumentation(self, current_depth):
        current_doc = u""

        #print 'current documentation_element =', self._id #type(self.documentation_element)
        #print self.documentation_element
        if not self.documentation_element is None:
            current_doc += self.documentation_element.createDocumentationBegin(current_depth)

        children = self.parent_to_child_relationship[self._id]
        for child_id in children:
            current_doc += self.get_node_with_id(child_id).createDocumentation(current_depth + 1)

        if not self.documentation_element is None:
            current_doc += self.documentation_element.createDocumentationEnd(current_depth)

        #print current_doc
        return current_doc
