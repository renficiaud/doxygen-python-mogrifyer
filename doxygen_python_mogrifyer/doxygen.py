# -*- coding: UTF-8 -*-

import os
from xml.dom.minidom import parseString

from utils import getText
from fix_xml import fixXML

from page import DoxygenPage
from file import DoxygenFile
from sectionfile import DoxygenSection
from filespage import registerDir, registerGroup, DoxygenFilesPage
from globalspage import registerGlobals, DoxygenGlobalsPage
from member_function import DoxygenMemberFunction

from documentation_node import DocumentationNode

class Doxygen(object):
  def __init__(self, options):
    self.options = options
    self.files = []
    self.staticfiles = []
    self.links = {}
    self.footer = {}
    if self.options.no_labels:
      self.labels = []
    elif self.options.labels == []:
      self.labels = ["Doxygen"]
    else:
      self.labels = options.labels

    self.references = {}

  def processFile(self, xml, parent_documentation_node=None):

    if parent_documentation_node is None:
      parent_documentation_node = DocumentationNode()

    compound_nodes = [(node, node.attributes["refid"].value, node.attributes["kind"].value)
                      for node in xml.documentElement.getElementsByTagName("compound")]
    pages = [i[1] for i in compound_nodes if i[2] == "page"]
    files = [i[1] for i in compound_nodes if i[2] == "file"]
    dirs = [i[1] for i in compound_nodes if i[2] == "dir"]
    groups = [i[1] for i in compound_nodes if i[2] == "group"]

    if xml.documentElement.tagName == "doxygenindex":

      if 0:
        # all special pages
        for f in pages:
          if options.verbose:
            print "Processing page", f + ".xml"
          self.processFile(parseString(fixXML(open(self.options.docs + f + ".xml", "r").read())),
                           parent_documentation_node)

        for f in files:
          if options.verbose:
            print "Processing file", f + ".xml"

          ff = os.path.join(options.docs, f + ".xml")
          if(not os.path.exists(ff)):
            print 'skipping inexistent file', ff
            continue
          self.processFile(parseString(fixXML(open(ff, "r").read())),
                           parent_documentation_node)

        for f in dirs:
          if options.verbose:
            print "Processing directory", f + ".xml"
          self.processFile(parseString(fixXML(open(self.options.docs + f + ".xml", "r").read())),
                           parent_documentation_node)

      # groups
      print 'Groups are', '\n-\t'.join([''] + groups)
      for f in groups:  # pour tests
        if self.options.verbose:
          print "Processing group", f, "-- from file", f + ".xml"
        self.processFile(parseString(fixXML(open(self.options.docs + f + ".xml", "r").read())),
                         parent_documentation_node)

      # for c in xml.documentElement.getElementsByTagName("compound"):
      #  registerGlobals(c)

    elif xml.documentElement.tagName == "doxygen":

      compounds = xml.documentElement.getElementsByTagName("compounddef")

      for c in compounds:

        if c.attributes["kind"].value == "file":
          self.footer["file"] = True
          self.files.append(DoxygenFile(c, self.options))

        elif c.attributes["kind"].value == "page":
          self.files.append(DoxygenPage(c, self.options))

        elif c.attributes["kind"].value == "dir":
          registerDir(c)

        elif c.attributes["kind"].value == "group":
          registerGroup(c)  # in order to register the parent->children relationship
          current_node = DocumentationNode()
          parent_documentation_node.add_child_node(current_node)
          current_section = DoxygenSection(c, self.options, current_node)
          current_node.node = current_section
          self.files.append(current_section)
        else:
          raise SystemError, "Unrecognized compound type. (%s)" % (c.attributes["kind"].value,)
    else:
      raise SystemError, "Unrecognized root file node. (%s)" % (xml.documentElement.tagName,)

  def addLink(self, refid, linkto):
      self.links[refid] = linkto

  def createFiles(self):
    files = []
    for f in self.files:
      files += f.createFiles()

    if self.footer.has_key("file"):
      files += DoxygenFilesPage(self.options).createFiles()

    if self.footer.has_key("globals"):
      files += DoxygenGlobalsPage(self.options).createFiles()
    return files + self.staticfiles

  def getFooter(self):
    footer = "---\n|| [%s Main Page]" % (self.options.prefix,)
    if self.footer.has_key("file"):
        footer += " || [%s_files Files]" % (self.options.prefix,)
    footer += " ||\n"
    return footer

  def copyFile(self, type, _from, _to):
    if self.options.output + _to not in [x[1] for x in self.staticfiles]:
        open(self.options.output + _to, "wb").write(open(_from, "rb").read())
        self.staticfiles.append(("static", _to, None))

