# -*- coding: UTF-8 -*-

from utils import getText

from .documentation_node import DocumentationNode

descriptions = {}
files = []

groups = {}  # group and list of parents


def registerDir(xml):
    dirname = getText(xml.getElementsByTagName("compoundname")[0].childNodes)
    for f in xml.getElementsByTagName("innerfile"):
        files.append((dirname, f.attributes["refid"].value, getText(f.childNodes)))


def registerGroup(xml):
    groupid = xml.attributes["id"].value  #getText(xml.getElementsByTagName("compounddef")[0].childNodes)

    DocumentationNode.register_id_type(groupid, DocumentationNode.node_type_group)

    if(not groups.has_key(groupid)):
        groupdesc = getText(xml.getElementsByTagName("title")[0].childNodes)

        groups[groupid] = {'description': groupdesc, 'children':[]}

    current_children = groups[groupid]['children']
    for f in xml.getElementsByTagName("innergroup"):

        child_id = f.attributes["refid"].value
        current_children.append(child_id)

        DocumentationNode.register_parent_to_child_relationship(groupid, child_id)
        DocumentationNode.register_id_type(child_id, DocumentationNode.node_type_group)


def registerFileBriefDescription(file, desc):
    descriptions[file] = desc


class DoxygenFilesPage(object):

    def __init__(self, options):
        self.options = options
        pass

    def createFiles(self):
        return [("wiki",
                 self.options.prefix + "_files",
                 FilesPage(searchList={"summary": "A List of files with brief descriptions.",
                                       "labels": self.options.labels,
                                       "prefix": self.options.prefix,
                                       "files": files,
                                       "descriptions": descriptions}))]

