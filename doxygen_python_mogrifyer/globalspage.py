# -*- coding: UTF-8 -*-

from utils import getText

members = []


def registerGlobals(xml):
    filename = getText(xml.getElementsByTagName("name")[0].childNodes)
    for f in xml.getElementsByTagName("member"):
        doxygen.footer["globals"] = True
        members.append((f.attributes["refid"].value,
                        getText(f.getElementsByTagName("name")[0].childNodes),
                        (xml.attributes["refid"].value, filename)))


class DoxygenGlobalsPage(object):

    def __init__(self, options):
        self.options = options
        pass

    def createFiles(self):
        return [("wiki",
                 self.options.prefix + "_globals",
                 GlobalsPage(searchList=
                             {"summary": "A list of all functions, variables, defines, enums, "\
                              "and typedefs with links to the files they belong to.",
                              "labels": self.options.labels,
                              "prefix": self.options.prefix,
                              "members": members}))]

