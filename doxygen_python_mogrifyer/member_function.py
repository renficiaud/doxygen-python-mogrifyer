# -*- coding: UTF-8 -*-


from utils import getText, getDirectDescendents
from text_elements import convertLine, convertLine_no_special_node

from documentation_node import DocumentationElement, DocumentationNode


# deprecated
class DoxygenMemberFunction(DocumentationElement):
  def __init__(self, xml, options, this_node):
    try:
      self.refid = xml.attributes["refid"].value
    except KeyError:
      self.refid = xml.attributes["id"].value
    
    this_node.set_id(self.refid)
    self.name = getText(xml.getElementsByTagName("name")[0].childNodes)

    if options.verbose:
      print "Parsing documentation for function", self.name

    l = [""]
    [x.getLines(l) for x in convertLine(xml.getElementsByTagName("briefdescription")[0], self, options, this_node)]
    self.brief = "".join(l).strip()

    l = [""]
    [x.getLines(l) for x in convertLine(xml.getElementsByTagName("detaileddescription")[0], self, options, this_node)]
    self.detailed = "".join(l).strip()

    #raw_input("detailed " + self.detailed)
    #raw_input("brief " + self.brief)

    #raw_input("0 " + xml.attributes["kind"].value)
    #raw_input("1 " + xml.nodeName)
    #raw_input("2 " + xml.toxml()  )
    #raw_input("3 " + xml.parentNode.toxml()   )
    #raw_input("4 " + xml.parentNode.nodeName   )
    #raw_input("5 " + repr([i for i in xml.getElementsByTagName("type")]))
    self.type = xml.attributes["kind"].value #getText(xml.getElementsByTagName("type")[0].childNodes)
    self.params = []
    
    self.protection = None
    try:
      self.protection = xml.attributes["prot"].value
    except e:
      pass
    
    self.is_virtual = False
    try:
      self.is_virtual = xml.attributes["virt"].value == 'virtual' # a v�rifier
    except Exception, e:
      pass
    
    
    if(self.name in problematic_names):
      return
    
    for p in xml.getElementsByTagName("param"):
        #raw_input("2 " + p.toxml()  )
        if(self.type == 'define'):
          type = "none"
          declname = getText(p.getElementsByTagName("defname")[0].childNodes)
        else:
          try:
            type = getText(p.getElementsByTagName("type")[0].childNodes)
            declname = getText(p.getElementsByTagName("declname")[0].childNodes)
          except Exception, e:
            declname = "unknown"
            type = "unknown"
            pass
        self.params.append((type, declname))
      #self.type = getText(xml.getElementsByTagName("type")[0].childNodes)

    def __get_sig(self):
        return self.type + " " + self.name + "(" + ", ".join(["%s %s" % x for x in self.params]) + ")"
    sig = property(__get_sig)

    def __get_doc(self):
        return unicode(Function(searchList={"f": self}))
    doc = property(__get_doc)


class DoxygenMember(DocumentationElement):
  """This class is used to describes a documentation entity of a section. It may contain 
   - a free function
   - a typedef
   - ...
   """ 
  def __init__(self, xml, options, this_node):
    try:
      self.refid = xml.attributes["refid"].value
    except KeyError:
      self.refid = xml.attributes["id"].value
    
    this_node.set_id(self.refid)
    self.name = getText(xml.getElementsByTagName("name")[0].childNodes)

    if options.verbose:
      print "Parsing documentation for member", self.name

    if options.verbose:
      print "-parsing brief"
    l = [""]
    [x.getLines(l) for x in convertLine(xml.getElementsByTagName("briefdescription")[0], self, options, this_node)]
    self.brief = "".join(l).strip()

    if options.verbose:
      print "-parsing detailed"
    l = [""]
    [x.getLines(l) for x in convertLine(xml.getElementsByTagName("detaileddescription")[0], self, options, this_node)]
    self.detailed = "".join(l).strip()

    self.type = xml.attributes["kind"].value #getText(xml.getElementsByTagName("type")[0].childNodes)
    self.params = []
    
    self.protection = None
    try:
      self.protection = xml.attributes["prot"].value
    except Exception, e:
      pass
    
    self.is_virtual = False
    try:
      self.is_virtual = xml.attributes["virt"].value == 'virtual' # à vérifier
    except Exception, e:
      pass
    
    
    if(self.name in problematic_names):
      return
    
    
    # parameters of the function, as detailed in the param child nodes
    # these parameters do involve only the type, the name, and an optional default value
    # it does not include the documentation of these variables at all
    # todo: mode to an appropriate function
    if options.verbose:
      print "-parsing direct parameters"

    for p in getDirectDescendents(xml, "param"):
      
      default_value = None
      if(self.type == 'define'):
        type = "none"
        declname = getText(p.getElementsByTagName("defname")[0].childNodes)
      else:
        try:
          # it should be a type
          type = getText(p.getElementsByTagName("type")[0].childNodes)
          
          # it could be a name
          try:
            declname = getText(p.getElementsByTagName("declname")[0].childNodes)
          except Exception,e:
            declname = None
          
          # it could be a default value
          try:
            default_value = getText(p.getElementsByTagName("defval")[0].childNodes)
          except Exception, e:
            pass
        
        except Exception, e:
          declname = "unknown"
          type = "unknown"
          pass
      self.params.append((type, declname, default_value))
      print "decl name", (type, declname, default_value)
    
    
    # this is the description of the parameters
    # todo: this is now handled by the detailed description parsing. It would be convenient to have
    # an id refering to the subtree of this parameter list.
    if options.verbose:
      print "-parsing direct parameterlist"
    for p in getDirectDescendents(xml, "detaileddescription"):
      
      parameterlist = p.getElementsByTagName("parameterlist")
      
      if(not (parameterlist is None)):
        if(len(parameterlist) > 0 and parameterlist[0].attributes["kind"].value == "param"):
      
          for k in parameterlist[0].getElementsByTagName("parameteritem"):#.childNodes:
            
            # we need the name
            name = getText(k.getElementsByTagName("parametername")[0].childNodes)
            
            l = [""]
            [x.getLines(l) for x in convertLine(k.getElementsByTagName("parameterdescription")[0], self, options, this_node)]
            
            convertlines_ = convertLine(k.getElementsByTagName("parameterdescription")[0], self, options, this_node)
            description = convertlines_#"".join(l).strip()
            
            print "Function/method %s, parameter %s, description : %s "%(self.name, name, description)
            pass
        pass
      pass

    def __get_sig(self):
      return self.type + " " + self.name + "(" + ", ".join(["%s %s" % x for x in self.params]) + ")"

    sig = property(__get_sig)

    def __get_doc(self):
      if(self.type == 'typedef'):
        return '.. cpp:type:: %s' % self.name
      
      elif self.type == 'function':
        # manque type retour
        out = '.. cpp:function:: %s ' % self.name
        out += '\n' + self.brief
        out += '\n\n' + self.detailed
        
      
      return "blablabla"
      #return unicode(Function(searchList={"f": self}))
      
    doc = property(__get_doc)



# incorrectly parsed stuff from Doxygen
problematic_names = ['BOOST_MPL_HAS_XXX_TRAIT_NAMED_DEF']


