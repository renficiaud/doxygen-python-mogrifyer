# -*- coding: UTF-8 -*-


from utils import getText

from text_elements import convertLine
from member_function import DoxygenMemberFunction


from documentation_node import DocumentationElement, DocumentationNode


class DoxygenPage(DocumentationElement):
  def __init__(self, xml, options, this_node):

    super(DoxygenPage, self).__init__(node=this_node, xmlroot=xml, options=options)

    self.node.id = xml.attributes["id"].value
    if self.node.id == "indexpage":
      self.pagename = options.prefix
    else:
      self.pagename = options.prefix + "_" + self.node.id

    print 'processing page', self.pagename
    try:
      title = xml.getElementsByTagName("title")[0]
    except IndexError:
      self.title = ""
    else:
      self.title = getText(title.childNodes)


    self.get_direct_children_and_tags()
    self.get_detailed()

  def createFiles(self):
    lines = [""]
    for d in self.detailed:
      d.getLines(lines)
    return [("wiki",
             self.pagename,
             Page(searchList={"summary": "", "labels": doxygen.labels, "page": "".join(lines)}))]

