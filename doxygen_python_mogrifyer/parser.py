
import os
from xml.dom.minidom import parseString


from fix_xml import fixXML

from documentation_node import DocumentationNode

from page import DoxygenPage
from sectionfile import DoxygenSection


from file import DoxygenFile
from filespage import registerDir, registerGroup, DoxygenFilesPage
from globalspage import registerGlobals, DoxygenGlobalsPage
from member_function import DoxygenMemberFunction


def _get_xml_file_content(filename_id, options):

  content = open(os.path.join(options.docs, filename_id + ".xml"), "r").read()
  return parseString(fixXML(content))

def parser(xml, options, parent_documentation_node):
   
  assert(not parent_documentation_node is None) 
    
  compound_nodes = [(node, node.attributes["refid"].value, node.attributes["kind"].value) 
                    for node in xml.documentElement.getElementsByTagName("compound")]
  pages = [i[1] for i in compound_nodes if i[2] == "page"]
  files = [i[1] for i in compound_nodes if i[2] == "file"]
  dirs  = [i[1] for i in compound_nodes if i[2] == "dir"]
  groups= [i[1] for i in compound_nodes if i[2] == "group"]
    
  if xml.documentElement.tagName == "doxygenindex":

    for f in pages:
      if options.verbose: 
        print "Processing page", f + ".xml"
      parser(_get_xml_file_content(f, options),
             options,
             parent_documentation_node)
    
    if 0:
      # all special pages        

              
      for f in files:
        if options.verbose: 
          print "Processing file", f + ".xml"
        
        ff = os.path.join(options.docs, f + ".xml")
        if(not os.path.exists(ff)):
          print 'skipping inexistent file', ff
          continue
        parse(parseString(fixXML(open(ff, "r").read())),
              options,
              parent_documentation_node)
          
      for f in dirs:
        if options.verbose:
          print "Processing directory", f + ".xml"
        parse(_get_xml_file_content(f, options),
              options,
              parent_documentation_node)
    
    # groups
    print 'Groups are', '\n-\t'.join([''] + groups)
    for f in groups:
      if options.verbose:
        print "Processing group", f, "-- from file", f + ".xml"
      parser(_get_xml_file_content(f, options),
             options,
             parent_documentation_node)
    
    #for c in xml.documentElement.getElementsByTagName("compound"):
    #  registerGlobals(c)
          
  elif xml.documentElement.tagName == "doxygen":
    
    compounds = xml.documentElement.getElementsByTagName("compounddef")
    
    for c in compounds:
      
      if c.attributes["kind"].value == "file":
        #self.footer["file"] = True
        #self.files.append(DoxygenFile(c, options))
        pass
      
      elif c.attributes["kind"].value == "page":
        current_node = DocumentationNode()
        
        current_section = DoxygenPage(c, options, current_node)
        print 'parent to child'
        print ' - parent id', parent_documentation_node.id
        print ' - child id', current_node.id

        # this should take place after the page is created (in which the id is also extracted)
        parent_documentation_node.add_child_node(current_node) 
        raw_input('page added')

      
      elif c.attributes["kind"].value == "dir":
        registerDir(c)
      
      elif c.attributes["kind"].value == "group":
        #registerGroup(c) # in order to register the parent->children relationship
        current_node = DocumentationNode()
        current_section = DoxygenSection(c, options, current_node)
        parent_documentation_node.add_child_node(current_node)
        #current_node.node = current_section
        #self.files.append(current_section)
      else:
        raise SystemError, "Unrecognized compound type. (%s)" % (c.attributes["kind"].value,)
  else:
    raise SystemError, "Unrecognized root file node. (%s)" % (xml.documentElement.tagName,)
  
  
  
  return parent_documentation_node