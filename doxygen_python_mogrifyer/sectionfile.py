# -*- coding: UTF-8 -*-

from utils import getText, getDirectDescendents, make_rest_title
from member_function import DoxygenMemberFunction, DoxygenMember
from text_elements import convertLine, get_text_content
from classfile import DoxygenClass
from xml.dom.minidom import parseString
from fix_xml import fixXML

from documentation_node import DocumentationElement, DocumentationNode


class DoxygenSection(DocumentationElement):
  """This class describes a section in doxygen. A section could include different documentation entities that form a group"""
  def __init__(self, xml, options, this_node):
    
    super(DoxygenSection, self).__init__(node = this_node, xmlroot = xml, options = options)
    
    # get the id of the current section  
    try:
      self.refid = xml.attributes["refid"].value
    except KeyError:
      self.refid = xml.attributes["id"].value
      
    # the name of the group as referred by other entities 
    self.compoundname = getText(xml.getElementsByTagName("compoundname")[0].childNodes)
    
    # transfer the id to the node
    this_node.set_id(self.refid)
    
    # title
    self.name = getText(xml.getElementsByTagName("title")[0].childNodes)
    
    if options.verbose: 
      print u"Generating documentation for §%s / %s" % (self.name, self.refid)

    self.get_direct_children_and_tags()

    self.get_brief()
    self.get_detailed()

    
    print self.brief
    for i in self.brief:
      print i.createDocumentationBegin(0)
      print i.createDocumentation(0)
      print i.createDocumentationEnd(0)
    raw_input('brief')

    #l = [""]
    #[x.getLines(l) for x in convertLine(xml.getElementsByTagName("briefdescription")[0], self, options, this_node)]
    #self.brief = "".join(l).strip()

    #l = [""]
    #[x.getLines(l) for x in convertLine(xml.getElementsByTagName("detaileddescription")[0], self, options, this_node)]
    #self.detailed = "".join(l).strip()
        
    # classes have they own xml file that should be parsed
    self.class_members = []
    classes = [node.attributes["refid"].value for node in xml.getElementsByTagName("innerclass")]
    for c in classes:
      if options.verbose: 
        print "Parsing (inner)class %s in section %s" % (c + ".xml", self.name)
      
      xml2 = parseString(fixXML(open(options.docs + c + ".xml", "r").read()))
      if xml2.documentElement.tagName == "doxygen":  
        compounds = xml2.documentElement.getElementsByTagName("compounddef")
        for g in compounds: 
          current_node = DocumentationNode()
          current_compound_element = DoxygenClass(g, options, current_node)
          current_node.node = current_compound_element
          this_node.add_child_node(current_node)
          self.class_members.append(current_compound_element)
    
    # member definitions that are referenced directly in the current xml
    self.sections = {}
    sections = getDirectDescendents(xml, "sectiondef")
    for current_section in sections:
      
      current_type = current_section.attributes['kind'].value
      if(not self.sections.has_key(current_type)): 
        self.sections[current_type] = [] 

      if options.verbose: 
        print u"Parsing members \"%s\" from section %s" % (current_type, self.name)
      
      for node in current_section.getElementsByTagName("memberdef"):
        current_node = DocumentationNode()
        current_compound_element = DoxygenMember(node, options, current_node)
        this_node.add_child_node(current_node)
        current_node.node = current_compound_element
        self.sections[current_type].append(current_compound_element)
        
        
    print '- inner classes', [i.name for i in self.class_members]
    print '- inner sections'
    for k, v in self.sections.items():
      print '-\t', k
      for i in v:
        print '--\t', i.name
    raw_input('toto')
      
  def getFileName(self):
    """Returns the filename of the current class"""
    return self.refid.replace('/', '_') + '.txt'

      
      
  def createCode(self, level):
    
    typedefs = self.sections.get("typedef") #[i for i in self.members if i.type == 'typedef']
    #public_members = [i for i in self.members if i.type == 'function' and i.protection == 'public']
    #protected_members = [i for i in self.members if i.type == 'function' and i.protection == 'protected']
    class_members = self.class_members#[i for i in self.members if i.type == 'class']
    
    
    out = make_rest_title(self.name, level) + '\n'
    
    out += self.brief + '\n' * 2
    out += self.detailed + '\n' * 2
    
    
    out += make_rest_title("Classes", level+1)
    
    for i in class_members:
      out += i.createCode(level+2)
    
    
    return out  
    
    
    
    
    if len(self.member_funcs) == 0:
      memberfuncs = None
    else:
      memberfuncs = "== Public Member Functions ==\n"
      for f in self.member_funcs:
        memberfuncs += "  * %(name)s\n" % {"name": f.name, }

    code = """#summary Automatically Generated by Doxygen2gwiki

= %(name)s =

%(memberfuncs)s

""" % ({"name": self.name, "memberfuncs": memberfuncs})



    template_globals = "Section %(name)s\n\
    = Description = \n\
    %(description)"
    
    ret = template_globals % {'name':self.name, 'description': self.brief + '\n' + self.detailed}

    return (prefix + self.refid + ".wiki", ret)


  def createFiles(self):
    return (self.getFileName(),self.createCode(0))