# -*- coding: UTF-8 -*-

import os.path
from .utils import getText
from .documentation_node import DocumentationNode, DocumentationElement


all_links = {}


def get_text_content(xml_doc_node, options, current_parent_node):
  text = []
  child = xml_doc_node.firstChild
  while child is not None:
    current_doc_node = DocumentationNode()
    current_doc = None
    
    if child.nodeType == child.TEXT_NODE:
      current_doc = Text(child, options, current_doc_node)
      if current_doc.is_empty():
        current_doc = None
    else:
      try:
        e = elements[child.tagName]
        
      except KeyError:
        print "Unrecognized element,", child.tagName
        current_doc = Text(None, options, current_doc_node)
        current_doc.text = getText(child.childNodes).strip()
      else:
        print child.tagName
        current_doc = e(child, options, current_doc_node)

    if not current_doc is None:
      text.append(current_doc)
      #current_doc_node.node = current_doc
      current_parent_node.add_child_node(current_doc_node)

    child = child.nextSibling
  return text


def convertLine(xml_doc_node, parent, options, current_parent_node):
  text = []
  child = xml_doc_node.firstChild
  while child is not None:
    current_doc_node = DocumentationNode()
    current_doc = None
    
    if child.nodeType == child.TEXT_NODE:
      current_doc = Text(child, parent, options, current_doc_node)
      if current_doc.is_empty():
        current_doc = None
    else:
      try:
        e = elements[child.tagName]
      except KeyError:
        print "Unrecognized element,", child.tagName
        current_doc = Text(None, parent, options, current_doc_node)
        current_doc.text = getText(child.childNodes).strip()
      else:
        current_doc = e(child, parent, options, current_doc_node)

    if not current_doc is None:
      text.append(current_doc)
      current_doc_node.node = current_doc
      current_parent_node.add_child_node(current_doc_node)

    child = child.nextSibling
  return text

def convertLine_no_special_node(node, parent, options, current_parent_node):
  """Special version dropping some tags"""
  
  forbidden_tags = ["parameterlist"]
  special_forbidden_tags = [('simplesect', 'return'), ('simplesect', 'author')]
  
  
  text = []
  child = node.firstChild
  while child is not None:
    if child.nodeType == child.TEXT_NODE:
      text.append(Text(child, parent, options))
    else:
      
      if(child.tagName in forbidden_tags):
        child = child.nextSibling
        continue
      
      
      uu = [i for i in special_forbidden_tags if i[0] == child.tagName and i[1] == child.attributes['kind'].value]
      if(len(uu) > 0):
        print "Skipping tag", child.tagName
        child = child.nextSibling
        continue
      
      try:
        e = elements[child.tagName]
      except KeyError:
        print "Unrecognized element,", child.tagName
        t = Text(None, parent, options)
        t.text = getText(child.childNodes).strip()
        text.append(t)
      else:
        text.append(e(child, parent, options))

    child = child.nextSibling
  return text




class Ignore(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        pass

class PassThrough(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self, options, current_doc_node)

    def getLines(self, lines):
        [x.getLines(lines) for x in self.text]

class Text(DocumentationElement):
  """A pure text element"""
  def __init__(self, xml, options, current_doc_node):
    if xml is not None:
      self.text = xml.data.strip("\n").strip()

  def is_empty(self):
    return len(self.text) == 0

  def getLines(self, lines):
    if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
      lines.append(self.text.lstrip())
    else:
      lines[-1] = lines[-1] + self.text
            
  def createDocumentationBegin(self, depth):
    return ''
  def createDocumentationEnd(self, depth):
    return ''
  
  def createDocumentation(self, depth):
    return self.text          


class Highlight(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self, options, current_doc_node)

    def getLines(self, lines):
        l = [""]
        [x.getLines(l) for x in self.text]
        text = "*" + "".join(l) + "*"
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class Emphasis(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self, options, current_doc_node)

    def getLines(self, lines):
        l = [""]
        [x.getLines(l) for x in self.text]
        text = "_" + "".join(l) + "_"
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class SuperScript(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self, options, current_doc_node)

    def getLines(self, lines):
        l = [""]
        [x.getLines(l) for x in self.text]
        text = "^" + "".join(l) + "^"
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class SubScript(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self, options, current_doc_node)

    def getLines(self, lines):
        l = [""]
        [x.getLines(l) for x in self.text]
        text = ",," + "".join(l) + ",,"
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class Para(DocumentationElement):
  """Represents a paragraph"""
  def __init__(self, xml, options, current_doc_node):
    
    super(Para, self).__init__(node = current_doc_node,
                               xmlroot = xml, 
                               options = options)

    # recursive call to the conversion
    self.text = get_text_content(xml, options, current_doc_node)

  def getLines(self, lines):
    if not lines[-1] == "\n":
        lines[-1] = lines[-1] + "\n"
    [x.getLines(lines) for x in self.text]
    if not lines[-1] == "\n":
        lines[-1] = lines[-1] + "\n"
    lines.append("\n")
      
      
  def createDocumentationBegin(self, depth):
    return '<p>'
  def createDocumentationEnd(self, depth):
    return '</p>'
  
  def createDocumentation(self, depth):
    
    output = u''
    
    for i in self.text:
      output += i.createDocumentationBegin(depth + 1)
      output += i.createDocumentation(depth + 1)
      output += i.createDocumentationEnd(depth + 1)
    return output

class LineBreak(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        if len(lines[-1]) == 0:
            lines[-1] = "\n"
        elif not lines[-1].endswith("\n"):
            lines[-1] = lines[-1] + "\n"

class Sect1(DocumentationElement):
  """A section"""
  def __init__(self, xml, options, current_doc_node):
    super(Sect1, self).__init__(node = current_doc_node, 
                                xmlroot = xml, 
                                options = options)
    #self.parent = parent
    #while parent is not None and not hasattr(parent, "pagename"):
    #  parent = parent.parent
    #if parent is not None:
    #  doxygen.addLink(xml.attributes["id"].value, parent.pagename)
    #else:
    #  doxygen.addLink(xml.attributes["id"].value, None)
    
    print 'section'
    
    self.text = get_text_content(xml, options, current_doc_node)
    #self.text = convertLine(xml, self)

  def getLines(self, lines):
      [x.getLines(lines) for x in self.text]

class Sect2(DocumentationElement):
  """A subsection"""
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent
    while parent is not None and not hasattr(parent, "pagename"):
      parent = parent.parent
    if parent is not None:
      doxygen.addLink(xml.attributes["id"].value, parent.pagename)
    else:
      doxygen.addLink(xml.attributes["id"].value, None)
    self.text = convertLine(xml, self)

  def getLines(self, lines):
    [x.getLines(lines) for x in self.text]

class Verbatim(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self)

    def getLines(self, lines):
        if len(lines[-1]) == 0 or lines[-1][-1] != "\n":
            lines[-1] = lines[-1] + "\n"
        lines.append("{{{\n")
        for x in self.text:
            assert isinstance(x, Text)
            lines.append(x.text)
        if len(lines[-1]) == 0 or lines[-1][-1] != "\n":
            lines[-1] = lines[-1] + "\n"
        lines.append("}}}\n")

class ComputerOutput(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.text = convertLine(xml, self)

    def getLines(self, lines):
        l = [""]
        [x.getLines(l) for x in self.text]
        text = "`" + "".join(l) + "`"
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class Heading(DocumentationElement):
  """I do not know exactly this one"""
  def __init__(self, xml, options, current_doc_node):
    super(Heading, self).__init__(node = current_doc_node,
                                  options = options,
                                  xmlroot = xml)
    current_level = xml.attributes["level"].value
    test = get_text_content(xml, options, current_doc_node)
    print 'heading content level', int(current_level)
    for i in test:
      print '- - ', i.createDocumentation(0)

  def getLines(self, lines):
      l = [""]
      [x.getLines(l) for x in self.text]
      text = "= " + "".join(l) + " =\n"
      lines.append(text)

class ULink(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.url = xml.attributes["url"].value
        self.text = convertLine(xml, self)

    def getLines(self, lines):
        l = [""]
        [x.getLines(l) for x in self.text]
        text = "[" + self.url + " " + "".join(l) + "]"
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class Ref(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.ref = xml.attributes["refid"].value
        self.text = convertLine(xml, self)

    def getLines(self, lines):
        try:
            ref = all_links[self.ref]#doxygen.links[self.ref]# raffi: à tester
        except KeyError:
            ref = None
        l = [""]
        [x.getLines(l) for x in self.text]
        if ref:
            text = "[" + ref + " " + "".join(l) + "]"
        else:
            text = "".join(l)
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text)
        else:
            lines[-1] = lines[-1] + text

class ItemizedList(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.listitems = []
        child = xml.firstChild
        while child is not None:
            if child.nodeType == child.TEXT_NODE:
                pass
            elif child.tagName == "listitem":
                self.listitems.append(convertLine(child, self))
            else:
                print "Unknown node type in itemized list, " + child.tagName

            child = child.nextSibling

    def getLines(self, lines):
        if not lines[-1] == "\n":
            lines[-1] = lines[-1] + "\n"
        text = []
        for item in self.listitems:
            l = [""]
            [i.getLines(l) for i in item]
            text.append(" * " + "".join(l).strip() + "\n")
        lines.extend(text)

class OrderedList(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.listitems = []
        child = xml.firstChild
        while child is not None:
            if child.nodeType == child.TEXT_NODE:
                pass
            elif child.tagName == "listitem":
                self.listitems.append(convertLine(child, self, options, current_doc_node))
            else:
                print "Unknown node type in itemized list, " + child.tagName

            child = child.nextSibling

    def getLines(self, lines):
        if not lines[-1] == "\n":
            lines[-1] = lines[-1] + "\n"
        text = []
        fake = False
        for item in self.listitems:
            l = [""]
            [i.getLines(l) for i in item]
            text.append(l)
            if len(l) > 1:
                fake = True

        if fake:
            for i in range(len(text)):
                lines.append(" %i. " % (i+1) + "".join(text[i]).strip() + "\n")
        else:
            lines.extend(["# " + "".join(l).strip() + "\n" for l in text])

class TocList(DocumentationElement):
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent
    self.items = convertLine(xml, self, options, current_doc_node)

  def getLines(self, lines):
      [x.getLines(lines) for x in self.items]

class TocItem(DocumentationElement):
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent
    self.ref = xml.attributes["id"].value
    self.text = convertLine(xml, self, options, current_doc_node)

    def getLines(self, lines):
        try:
            ref = doxygen.links[self.ref]
        except KeyError:
            ref = None
        l = [""]
        [x.getLines(l) for x in self.text]
        if ref:
            text = "[" + ref + " " + "".join(l) + "] "
        else:
            text = "".join(l)
        if len(lines[-1]) > 0 and lines[-1][-1] == "\n":
            lines.append(text[1:])
        else:
            lines[-1] = lines[-1] + text

class ProgramListing(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.code = [convertLine(x, self) for x in xml.getElementsByTagName("codeline")]

    def getLines(self, lines):
        if len(lines[-1]) == 0 or lines[-1][-1] != "\n":
            lines[-1] = lines[-1] + "\n"
        for l in self.code:
            [x.getLines(lines) for x in l]
            if len(lines[-1]) == 0 or lines[-1][-1] != "\n":
                lines[-1] = lines[-1] + "\n"
        if len(lines[-1]) == 0 or lines[-1][-1] != "\n":
            lines[-1] = lines[-1] + "\n"
        lines.append("\n")

class Space(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
      self.parent = parent

    def getLines(self, lines):
      lines[-1] = lines[-1] + " "

class VariableList(DocumentationElement):
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent
    self.varitems = []
    self.listitems = []
    child = xml.firstChild
    while child is not None:
      if child.nodeType == child.TEXT_NODE:
        pass
      elif child.tagName == "listitem":
        self.listitems.append(convertLine(child, self, options, current_doc_node))
      elif child.tagName == "varlistentry":
        self.varitems.append(convertLine(child, self, options, current_doc_node))
      else:
        print "Unknown node type in itemized list, " + child.tagName

      child = child.nextSibling

    def getLines(self, lines):
        if not lines[-1] == "\n":
            lines[-1] = lines[-1] + "\n"
        text = []
        for vartext, item in zip(self.varitems, self.listitems):
            var = [""]
            [i.getLines(var) for i in vartext]
            l = [""]
            [i.getLines(l) for i in item]
            text.append("".join(var).strip() + "\n")
            text.append(" " + "".join(l).strip() + "\n")
        lines.extend(text)

class Image(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        if xml.attributes["type"].value == "html":
            image = self.findImage(xml.attributes["name"].value)
            if image:
                doxygen.copyFile("static", image, options.prefix + "_" + xml.attributes["name"].value)
            if options.project is None:
                print "Warning, to use images you *must* supply the project name using the -j option."
                self.url = None
            else:
                self.url = "http://%s.googlecode.com/svn/wiki/%s" % (options.project, options.prefix + "_" + xml.attributes["name"].value)
        else:
            self.url = None

    def findImage(self, name):
        if os.path.exists(options.output + name):
            return options.output + name
        if options.html and os.path.exists(options.html + name):
            return options.html + name
        for i in options.images:
            if os.path.exists(i + name):
                return i + name
        print "Couldn't find image %s." % (name, )
        print "Looked in..."
        for d in [options.output, options.html] + options.images:
            if d is not None:
                print d

    def getLines(self, lines):
        if self.url:
            lines[-1] = lines[-1] + "[" + self.url + "] "
        else:
            pass

class Formula(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        self.id = xml.attributes["id"].value
        self.text = convertLine(xml, parent)
        self.is_inline = False
        if(len(self.text)>0):
          self.is_inline = self.text[0][0] == "$" 
        
        # this is shit
        #image = self.findImage("form_" + id + ".png")
        #if image:
        #    doxygen.copyFile("static", image, options.prefix + "_" + "form_" + id + ".png")
        #if options.project is None:
        #    print "Warning, to use images you *must* supply the project name using the -j option."
        #    self.url = None
        #else:
        #    self.url = "http://%s.googlecode.com/svn/wiki/%s" % (options.project, options.prefix + "_" + "form_" + id + ".png")

    def getLines(self, lines):
      var = [""]
      [i.getLines(var) for i in self.text]
      
      # this is dependant upon the options
      if(self.is_inline):
        txt = "".join(var).strip()
        lines[-1] = lines[-1] + ':math:`' + txt + '`'
      else:
        lines[-1] = lines[-1] + '.. math::' + '\n'
        for i in var:
          lines[-1] = lines[-1] + '\t' + i.strip() + '\n'

class Copy(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        lines[-1] = lines[-1] + u"\xa9"

class NonBreakableSpace(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        lines[-1] = lines[-1] + u"\xf0"

class NDash(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        lines[-1] = lines[-1] + u"\u2013"

class MDash(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        lines[-1] = lines[-1] + u"\u2014"

class Acute(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "o":
            self.letter = u"\xf3"
        elif l == "O":
            self.letter = u"\xd3"
        elif l == "a":
            self.letter = u"\xe1"
        elif l == "A":
            self.letter = u"\xc1"
        else:
            print "Warning, %c is an unrecognized letter for Acute." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class Umlaut(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "o":
            self.letter = u"\xf6"
        elif l == "O":
            self.letter = u"\xd5"
        elif l == "a":
            self.letter = u"\xe4"
        elif l == "A":
            self.letter = u"\xc2"
        else:
            print "Warning, %c is an unrecognized letter for Umlaut." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class Grave(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "o":
            self.letter = u"\xf2"
        elif l == "O":
            self.letter = u"\xd2"
        elif l == "a":
            self.letter = u"\xe0"
        elif l == "A":
            self.letter = u"\xc0"
        else:
            print "Warning, %c is an unrecognized letter for Grave." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class Circ(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "a":
            self.letter = u"\xe2"
        elif l == "A":
            self.letter = u"\xc2"
        else:
            print "Warning, %c is an unrecognized letter for Circ." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class Tilde(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "a":
            self.letter = u"\xe3"
        elif l == "A":
            self.letter = u"\xc3"
        else:
            print "Warning, %c is an unrecognized letter for Tilde." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class Szlig(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent

    def getLines(self, lines):
        lines[-1] = lines[-1] + u"\xdf"

class Cedil(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "c":
            self.letter = u"\xe7"
        elif l == "C":
            self.letter = u"\xc7"
        else:
            print "Warning, %c is an unrecognized letter for Cedil." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class Ring(DocumentationElement):
    def __init__(self, xml, parent, options, current_doc_node):
        self.parent = parent
        l = xml.attributes["char"].value
        if l == "a":
            self.letter = u"\xe5"
        elif l == "A":
            self.letter = u"\xc5"
        else:
            print "Warning, %c is an unrecognized letter for Ring." % (l, )
            print "Please report this as a bug to the Doxygen2GWiki project."
            self.letter = ""

    def getLines(self, lines):
        lines[-1] = lines[-1] + self.letter

class ParameterList(DocumentationElement):
  """The tag used to define the parameter list for functions. It contains a list of "parameteritem".
  WARNING: a care should be taken for parameter current_node.node below"""
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent
    self.parameters = []
    for x in xml.getElementsByTagName("parameteritem"):
      print "parameter item"
      current_node = DocumentationNode()
      children_classes = convertLine(x, self, options, current_node)
      current_node.node = self#children_classes
      current_doc_node.children.append(current_node)
      
    
      self.parameters.append(children_classes)

    return
# 
# 
#     self.parameteritems = []
#     child = xml.firstChild
#     while child is not None:
#       if child.nodeType == child.TEXT_NODE:
#         #print "Getting a text node in ParameterList", child.data.strip("\n"), len(child.data.strip("\n"))
#         # happens, check to see if there might be a cross ref here
#         pass
#       elif child.tagName == "parameteritem":
#         current_node = DocumentationNode()
#         children_classes = convertLine(child, self, options, current_node)
#         current_node.node = children_classes
#         current_doc_node.children.append(current_node)
#         self.parameteritems.append(children_classes)
#       else:
#         print "Unknown node type in parameter list, " + child.tagName
# 
#       child = child.nextSibling


  def getLines(self, lines):
    pass

class ParameterItem(DocumentationElement):
  """The tag used to define the parameter list for functions. It contains a "parameternamelist" and a "parameterdescription"."""
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent

#     current_node = DocumentationNode()
#     children_classes = convertLine(child, self, options, current_node)
#     current_node.node = children_classes
#     current_doc_node.children.append(current_node)
#     #self.parameteritems.append(children_classes)
#     return

    self.parameteritems = []
    child = xml.firstChild
    while child is not None:
      if child.nodeType == child.TEXT_NODE:
        print "Getting a text node in ParameterItem", child.data.strip("\n")
        pass
      else:
        current_node = DocumentationNode()
        children_classes = convertLine(child, self, options, current_node)
        current_node.node = children_classes
        current_doc_node.children.append(current_node)
        self.parameteritems.append(children_classes)

      child = child.nextSibling
  def getLines(self, lines):
    pass
  
class ParameterNameList(DocumentationElement):
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent

    # this node just uses "parametername" as a child node. 
    # BUT maybe it is also used for reference
    convertLine(xml, self, options, current_doc_node)

    return

#     self.parameternames = []
#     child = xml.firstChild
#     while child is not None:
#       if child.nodeType == child.TEXT_NODE:
#         #print "Getting a text node in ParameterNameList", child.data.strip("\n"), len(child.data.strip("\n"))
#         # happens...
#         pass
#       
#       elif child.tagName == "parametername":
#         current_node = DocumentationNode()
#         children_classes = convertLine(child, self, options, current_node)
#         current_node.node = children_classes
#         current_doc_node.children.append(current_node)        
#         self.parameternames.append(children_classes)
#       else:
#         print "Unknown node type in parameter list, " + child.tagName
# 
#       child = child.nextSibling
#     
#     if len(self.parameternames) > 1:
#       print "Several parameter names found..."
#     print "parameter names =", repr(self.parameternames)
      
  def getLines(self, lines):
    pass
  
  
class ParameterName(DocumentationElement):
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent

    try:
      self.direction = xml.attributes["direction"].value
    except KeyError:
      self.direction = None

    self.name = None
    child = xml.firstChild
    while child is not None:
      if child.nodeType == child.TEXT_NODE:
        if not self.name is None:
          print "Several names for this parameter"
        self.name = child.data.strip("\n")
        pass
      child = child.nextSibling

    if not self.name is None:
      print " - parameter [%s]%s" % (self.direction if not self.direction is None else "unknown", self.name)
      
  def getLines(self, lines):
    pass

class ParameterDescription(DocumentationElement):
  def __init__(self, xml, parent, options, current_doc_node):
    self.parent = parent
    self.description = convertLine(xml, self, options, current_doc_node)
    
    print " - parameter description", self.description

  def getLines(self, lines):
    pass




elements = {
    "highlight": Highlight,
    "bold": Highlight,
    "emphasis": Emphasis,
    "para": Para,
    "sect1": Sect1,
    "sect2": Sect2,
    "anchor": Ignore,
    "htmlonly": Ignore,
    "latexonly": Ignore,
    "hruler": Ignore,
    "indexentry": Ignore,
    "verbatim": Verbatim,
    "preformatted": Verbatim,
    "computeroutput": ComputerOutput,
    "heading": Heading,
    "title": Heading,
    "ulink": ULink,
    "ref": Ref,
    "toclist": TocList,
    "tocitem": TocItem,
    "itemizedlist": ItemizedList,
    "orderedlist": OrderedList,
    "simplesect": Para,
    "linebreak": LineBreak,
    "programlisting": ProgramListing,
    "superscript": SuperScript,
    "subscript": SubScript,
    "sp": Space,
    "variablelist": VariableList,
    "varlistentry": Ignore,
    "term": PassThrough,
    "image": Image,
    "formula": Formula,
    "center": PassThrough,
    "copy": Copy,
    "umlaut": Umlaut,
    "acute": Acute,
    "grave": Grave,
    "nonbreakablespace": NonBreakableSpace,
    "ndash": NDash,
    "mdash": MDash,
    "circ": Circ,
    "tilde": Tilde,
    "szlig": Szlig,
    "cedil": Cedil,
    "ring": Ring,
    "parameterlist" : ParameterList,
    "parameteritem" : ParameterItem,
    "parameternamelist" : ParameterNameList,
    "parametername" : ParameterName,
    "parameterdescription" : ParameterDescription,
    
    
}
