# -*- coding: UTF-8 -*-


def getText(nodelist):
    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc = rc + node.data
        elif node.nodeType == node.ELEMENT_NODE:
            try:
                if(node.attributes["kindref"].value == "compound" and node.nodeName == "ref"):
                    rc += getText(node.childNodes)  #[0].data#getText(getDirectDescendents(node, "ref"))
            except Exception, e:
                pass

    return rc


def getDirectDescendents(node, tagname):
    return [n for n in node.getElementsByTagName(tagname) if n.parentNode is node]


def make_rest_title(title_content, level):
    """Test to understand how rest works ..."""
    s = '=' * len(title_content)
    return '\t' * level + title_content + '\n' + '\t' * level + s


def make_rest_table(table_contents):
    """First element is taken as the titles"""
    columns_widths = [max([len(j[i]) for j in table_contents]) for i in range(len(table_contents[0]))]

    s = "=" * columns_widths[0]
    t = "%.s" % table_contents[0]
    for i in columns_widths[1:]:
        s += " "
        s += "=" * i
