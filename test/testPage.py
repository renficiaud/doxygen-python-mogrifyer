import os
import sys
import unittest
from xml.dom.minidom import parse

from doxygen_python_mogrifyer.parser import parser
from doxygen_python_mogrifyer.documentation_node import DocumentationNode

test_directory = os.path.join(os.path.dirname(__file__), os.pardir, "data", "simpletest1", "xml", "xml")


class PageDiscoverTest(unittest.TestCase):

    def setUp(self):

        class dumm(object):
            pass

        option = dumm()
        option.verbose = True
        option.docs = test_directory + '/'
        option.prefix = 'testsimple1'

        xml_content = parse(open(os.path.join(test_directory, 'index.xml')))
        self.root_node = DocumentationNode()
        self.root_node.id = 0
        parser(xml_content, option, self.root_node)

    def tearDown(self):
        DocumentationNode.clear_state()

    def testNbChildren(self):
        """test the proper discovery of the page"""

        self.assertEqual(len(self.root_node.children_ids), 1)

    def testChildrenIds(self):
        """test the proper understanding of the ids"""
        self.assertIn("indexpage", self.root_node.children_ids)
